﻿using System;
using System.Windows.Forms;

namespace lab2
{
    sealed class MyEventArgs: EventArgs
    {
        public readonly String surname;
        public readonly String name;
        public readonly String secName;
        public readonly Form form;
    }
}
