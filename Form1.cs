﻿using System;
using System.IO;
using System.Data.SqlClient;
using System.Windows.Forms;


namespace lab2
{
    public partial class Form1 : Form, IDisposable
    {
        private readonly SqlConnection conn = new SqlConnection("Data Source=(local);Initial Catalog=db-labs-2sem;Integrated Security=true");

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public void Dispose() 
        {
            base.Dispose();
        }
        private void readButton_click(object _, EventArgs e)
        {
            conn.Open();
            SqlCommand command = new SqlCommand("SELECT * FROM Employees", this.conn);
            try
            {
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    String surname = reader.GetString(1);
                    String name = reader.GetString(2);
                    String secName = reader.GetString(3);
                    ListViewItem listViewItem = new ListViewItem();
                    listView1.Items.Add(String.Join("   ", surname, name, secName));

                }
            }
            catch (Exception ex)
            {
                TextWriter writer = Console.Error;
                writer.WriteLine(ex.ToString());
            }
            finally {
                conn.Close();
            }

            }

        private void clearArea_click(object _, EventArgs __)
        {
            listView1.Items.Clear();
        }

        private void buttonTotal_click(object _, EventArgs __)
        {
            conn.Open();
            SqlCommand command = new SqlCommand("SELECT COUNT(*) FROM Employees", conn);
            int totalItems = (int)command.ExecuteScalar();
            label2.Text = "Всего сотрудников - " + totalItems;
            conn.Close();
        }

        private void addEmployee_click(object _, EventArgs __)
        {
            try
            {
                Form insertEmpWindow = new Form();
                insertEmpWindow.Location = new System.Drawing.Point(430, 200);
                insertEmpWindow.Size = new System.Drawing.Size(300, 300);
                insertEmpWindow.ResumeLayout(false);
                insertEmpWindow.PerformLayout();

                Button buttonAddLine = new Button();
                buttonAddLine.Location = new System.Drawing.Point(100, 200);
                buttonAddLine.Size = new System.Drawing.Size(99, 41);
                buttonAddLine.Text = "Добавить";
                buttonAddLine.UseVisualStyleBackColor = true;

                TextBox surname = new TextBox();
                Label surnameLabel = new Label();
                surnameLabel.Location = new System.Drawing.Point(50, 30);
                surnameLabel.Size = new System.Drawing.Size(70, 17);
                surnameLabel.Text = "Фамилия";
                surname.Location = new System.Drawing.Point(120, 30);
                surname.Size = new System.Drawing.Size(125, 30);

                TextBox name = new TextBox();
                Label nameLabel = new Label();
                nameLabel.Location = new System.Drawing.Point(50, 70);
                nameLabel.Size = new System.Drawing.Size(70, 17);
                nameLabel.Text = "Имя";
                name.Location = new System.Drawing.Point(120, 70);
                name.Size = new System.Drawing.Size(125, 30);

                TextBox secName = new TextBox();
                Label secNameLabel = new Label();
                secNameLabel.Location = new System.Drawing.Point(50, 110);
                secNameLabel.Size = new System.Drawing.Size(70, 17);
                secNameLabel.Text = "Отчество";
                secName.Location = new System.Drawing.Point(120, 110);
                secName.Size = new System.Drawing.Size(125, 30);

                
                insertEmpWindow.Controls.Add(buttonAddLine);
                insertEmpWindow.Controls.Add(surname);
                insertEmpWindow.Controls.Add(surnameLabel);
                insertEmpWindow.Controls.Add(name);
                insertEmpWindow.Controls.Add(nameLabel);
                insertEmpWindow.Controls.Add(secName);
                insertEmpWindow.Controls.Add(secNameLabel);

                buttonAddLine.Click += (sender, e) => {
                    conn.Open();
                    SqlCommand command = new SqlCommand("insert_emp", this.conn);
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@SURNAME", surname.Text);
                    command.Parameters.AddWithValue("@NAME", name.Text);
                    command.Parameters.AddWithValue("@SECNAME", secName.Text);
                    command.Parameters.Add(new SqlParameter("@EmployeeID", System.Data.SqlDbType.Int, 4));
                    command.Parameters["@EmployeeID"].Direction = System.Data.ParameterDirection.Output;
                    command.ExecuteNonQuery();

                    newEmpInfo.Text = "Новому сотруднику присвоен ID " + (int)command.Parameters["@EmployeeID"].Value;
                    insertEmpWindow.Close();
                    conn.Close();
                };

                insertEmpWindow.Show();
            }
            catch (Exception ex)
            {
                var stdErr = Console.Error;
                stdErr.WriteLine(ex);
            }
        }
        private void addEmployeeInsideNewWin_click(object sender, MyEventArgs e) {
          
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
